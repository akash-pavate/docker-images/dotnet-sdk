FROM mcr.microsoft.com/dotnet/sdk:5.0

RUN apt-get update && apt-get install -y \
zip \
curl

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash
RUN apt-get install -y nodejs

RUN npm install -g npm@latest

RUN npm install -g @angular/cli
